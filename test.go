package main

import (
	"context"
	"fmt"
	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/clientv3/concurrency"
	"github.com/google/uuid"
	"log"
	"time"
	//"math/rand"
	//"strconv"
)

var (
	client *clientv3.Client
	session *concurrency.Session
	election *concurrency.Election
	name string
	ctx context.Context
)

func main() {
	client, err := clientv3.New(clientv3.Config{Endpoints: []string{"localhost:2379"}})
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	session, err := concurrency.NewSession(client)
	if err != nil {
		log.Fatal(err)
	}

	election := concurrency.NewElection(session, "/leader-election")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	//rand.Seed(time.Now().UnixNano())
	//name := "e" + strconv.Itoa(rand.Intn(200))
	name := uuid.NewString()

	iAmLeader := make(chan struct{})

	go func() {
		if err := election.Campaign(ctx, ""); err != nil {
			log.Fatal(err)
		}
		close(iAmLeader)
	}()

	tick := time.NewTicker(3 * time.Second)
	ticksCounter := 0

	for {
		select {
			case <-tick.C:
				select {
					case <-iAmLeader:
						fmt.Println("I am leader: " + name)
						ticksCounter++
						if ticksCounter >= 3 {
							session.Close()
						}
					default:
						fmt.Println("I am follower: " + name)
				}
			case <-session.Done():
				fmt.Println("Session done: " + name)
				return
		}
	}
}
